# TEL19A - Grundlagen der Informatik

Dieses Repository enthält Vorlesungsmaterial und Code für die Vorlesungen Informatik 1+2 im Kurs TEL19A an der DHBW Mannheim.

Der Code zur Vorlesung kann hier heruntergeladen werden oder dieses Repository kann einfach auf dem privaten Rechner geklont werden
(dazu wird das Versionskontrollsystem **Git** benötigt).
Die Folien liegen hier als Quellcode vor. Wer diese als PDF benötigt und/oder wer die interaktiven Jupyter-Notebooks aus der Vorlesung benutzen möchte,
kann dazu den folgenden Binder-Link verwenden:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/reinerh%2Fdhbw%2Ftel19a/master?urlpath=lab)

**Warnung:**
Die Notebooks sind zwar interaktiv, die Binder-Sitzung bleibt aber nicht dauerhaft erhalten.
D.h. wenn Sie dort Änderungen machen, die Sie behalten wollen, müssen Sie unbedingt Ihre Daten herunterladen, bevor Sie den Browser schließen oder offline gehen.

**Für Fortgeschrittene:**
Wer das Repository klont, kann die Folien auch selbst bauen, dazu ist das Textsatzsystem **LaTeX** notwendig.
Jupyter gibt es auch für den lokalen Einsatz, am Besten installiert man sich dazu **Anaconda** und den C++-Kernel **Xeus-Cling**.
Das ist allerdings leider nur unter Linux und MacOS möglich.
Für die Vorlesung wird beides nicht notwendig sein, es ist aber evtl. lehrreich, es mal zu versuchen ;-).
