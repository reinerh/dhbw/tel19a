#ifndef VECSUM_H
#define VECSUM_H

template <class T>
T vecsum (std::vector<T> v)
{
    T result;
    result = v[0];
    for (int i=1; i<v.size(); i++) {
        result += v[i];
    }
    return result;
}

#endif

