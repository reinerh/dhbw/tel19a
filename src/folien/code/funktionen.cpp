#include<iostream>
using namespace std;

int foo(int a, int b)    // Prototyp:
// Rückgabetyp: 'int'
// Name: 'foo'
// Eingabe: Zwei Parameter vom Typ 'int'
                         
{                        // Anfang des Rumpfes
    while (b > 0) {
        a++; b--;
    }
    while (b < 0) {
        a--; b++;
    }
    return a;            // Ergebnis zurückgeben
}                        // Ende des Rumpfes

int main() {
    cout << foo(3,4);

    return 0;
}
