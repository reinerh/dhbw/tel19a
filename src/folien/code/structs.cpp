#include<iostream>
using namespace std;

struct complex {
    double real;
    double img;
};

struct complex add(complex x,
                   complex y) {
    complex result;
    result.real = x.real + y.real;
    result.img = x.img + y.img;
    return result;
}

int main() {
    struct complex c;
    c.real = 3; c.img = 2.3;
    cout << "c == " << c.real << " + " << c.img << "i" << endl;
    
    struct complex d = {4,1};
    struct complex e = add(c,d);
    cout << "d == " << d.real << " + " << d.img << "i" << endl;
    cout << "c+d == " << e.real << " + " << e.img << "i" << endl;

    struct complex f = { 23,56 };
    struct complex * q = &f;
    
    cout << "q == " << q->real << " + " << q->img << "i" << endl;
}


