#ifndef LOGIC_H
#define LOGIC_H

#include "common.h"

#include<string>

/** Startet das Spiel und fuehrt die angegeben Anzahl Runden aus.
    p1 und p2 sind die Namen der Spieler, rounds die gewuenschte Rundenzahl.
**/
void run(std::string p1, std::string p2, int rounds = 1);

/** Wertet die angegeben Spielzuege aus. **/
Result evaluate(Move m1, Move m2);


#endif