#ifndef COMMON_H
#define COMMON_H

/** Enum fuer die moeglichen Spielzuege **/
enum class Move {
    rock,
    paper,
    scissors
};

/** Enum fuer das Ergebnis eines Spiels **/
enum class Result {
    p1,
    p2,
    draw
};

#endif