#include "logic.h"

int main() {
    
    // Starte drei Spiele mit den angegebenen Spielernamen.
    run("Max Mustermann", "Monika Musterfrau", 3);
    
    return 0;
}