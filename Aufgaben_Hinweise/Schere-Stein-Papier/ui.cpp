#include "ui.h"

#include<iostream>
using namespace std;

std::string to_string(Move m) {
    // Liefern Sie zum gegebenen m den passenden String zurueck.
    // Wenn m == Move::Rock, dann "Stein" etc.
    
    // Standard-Ergebnis, damit die Funktion kompiliert werden kann.
    return "";
}

Move get_move(std::string player_name)
{
    // Lesen Sie hier die Eingabe ein. D.h. fragen Sie analog zu TicTacToe den Spieler
    // nach seiner Eingabe. Liefern Sie dann den passenden Move zurueck.
    
    /* Hinweis: Sie koennen dies komplett mit if oder switch loesen.
                Alternativ kann man folgendermassen int in Move umwandeln bzw, umgekehrt:
    
       static_cast<int>(m): Wandelt m nach int um.
       static_cast<Move>(i): Wandelt i nach Move um.
    */

    // Standard-Ergebnis, damit die Funktion kompiliert werden kann.
    return Move::rock;
}

void print_winner(std::string player_name)
{
    
}

void print_draw()
{
    
}