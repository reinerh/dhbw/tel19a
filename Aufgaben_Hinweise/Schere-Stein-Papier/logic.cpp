#include "logic.h"
#include "ui.h"

void run(std::string p1, std::string p2, int rounds)
{
    while (rounds > 0) {        
        Move m1 = get_move(p1);
        Move m2 = get_move(p2);
        
        Result res = evaluate(m1,m2);
        switch (res) {
            case Result::p1: print_winner(p1); break;
            case Result::p2: print_winner(p2); break;
            default: print_draw();
        }
        --rounds;
    }
}

Result evaluate(Move m1, Move m2)
{
    // Hier wird beispielhaft ein Ergebnis geliefert, damit die Funktion kompiliert werden kann.
    // Vergleichen Sie hier die gegebenen 'm1' und 'm2' und liefernt Sie das passende Ergebnis zurueck.
    // 'Result' und 'Move' sind Aufzaehlungsdatentypen, die in 'common.h' definiert sind.
    return Result::draw;
}