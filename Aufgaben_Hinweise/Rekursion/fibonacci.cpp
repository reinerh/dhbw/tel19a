#include<iostream>
using namespace std;

/*** AUFGABE: Suchen von Teilwörtern in einem String ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine rekursive Funktion `fib()`, die als Argument eine positive Zahl `n` erwartet.
Die Funktion soll die n-te Fibonacci-Zahl zurückliefern.
***/
unsigned int fib(unsigned int n) {
    // Legen Sie das Ergebnis für die Spezialfälle n==0 und n==1 fest.
    // In beiden Fällen muss das Ergebnis 1 sein.
    
    // Machen Sie den rekursiven Aufruf für den "Normalfall":
    // Berechnen Sie das Ergebnis aus Aufrufen für n-1 und n-2.
}
/*** TESTCODE/MAIN ***/
int main()
{
    cout << fib(0) << endl;      // Soll 1 ausgeben.
    cout << fib(1) << endl;      // Soll 1 ausgeben.
    cout << fib(2) << endl;      // Soll 2 ausgeben.
    cout << fib(3) << endl;      // Soll 3 ausgeben.
    cout << fib(4) << endl;      // Soll 5 ausgeben.
    cout << fib(5) << endl;      // Soll 8 ausgeben.
    cout << fib(6) << endl;      // Soll 13 ausgeben.
    
    return 0;
}
