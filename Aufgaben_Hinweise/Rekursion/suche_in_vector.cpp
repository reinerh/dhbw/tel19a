#include<iostream>
#include<vector>
using namespace std;

/*** AUFGABE: Suchen von Teilwörtern in einem String ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine rekursive Funktion `find()`, die als Argumente einen `vector<int>` und eine Zahl n erwartet.
Die Funktion soll n im Vektor suchen und ihre Position zurückliefern. Ist n nicht enthalten, soll die Länge des
Vektor zurückgegeben werden.
***/
int find(vector<int> v, int n) {
    // Rekursionsanfang: Behandeln Sie zwei Spezialfälle:
    // 1. n ist nicht enthalten.
    // 2. n steht an Stelle 0.
    
    // In beiden Fällen können Sie aussteigen und 0 liefern.
    
    // Das erste Element ist ab hier nicht n, da Sie sonst oben ausgestiegen wären.
    // Entfernen Sie nun das erste Element und suchen im verbleibenden Vektor weiter.
    // Zum Ergebnis müssen Sie 1 addieren, da sich beim Entfernen alle Positionen
    // um 1 verringern.
    
    // Zum Entfernen können Sie entweder unsere eigene `erase`-Funktion aus den Übungsaufgaben oder
    // einfach vector::erase benutzen. Siehe dazu die Cpp-API-Dokumentation, z.B. unter cppreference.com.
}
/*** TESTCODE/MAIN ***/
int main()
{
    cout << find({1,3,5,7,9},3)  << endl;      // Soll 1 ausgeben.
    cout << find({1,3,5,7,9},5)  << endl;      // Soll 2 ausgeben.
    cout << find({1,3,5,7,9},42)  << endl;     // Soll 5 ausgeben.
    cout << find({},3)  << endl;               // Soll 0 ausgeben.
    
    return 0;
}
