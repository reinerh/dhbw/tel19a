#include<iostream>
using namespace std;

/*** AUFGABE: Suchen von Teilwörtern in einem String ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine iterative Funktion `fib()`, die als Argument eine positive Zahl `n` erwartet.
Die Funktion soll die n-te Fibonacci-Zahl zurückliefern.

Anmerkung: Im Gegensatz zur anderen Fibonacci-Aufgabe sollen Sie hier keine Rekursion schreiben,
           sondern das gleiche mittels einer Schleife bewerkstelligen. D.h. Sie müssen sich
           überlegen, welche Zwischenergebnisse Sie von einem zum nächsten Schleifendurchlauf
           mitnehmen müssen. Dies soll verdeutlichen, warum Rekursion manchmal die bessere
           und einfachere Wahl ist.
***/
unsigned int fib(unsigned int n) {
    // Legen Sie sich zwei Hilfsvariablen an, in denen Sie immer die beiden letzten Werte
    // der Fibonacci-Folge speichern. Nun laufen Sie in einer Schleife bis n und addieren
    // die beiden Hilfsvariablen, um den nächsten Wert zu bekommen.
    
    // Wenn Sie die Hilfsvariablen z.B. x1 und x2 nennen und immer den allerletzten Wert
    // in x2 speichern, dann können Sie am Ende einfach x2 zurückliefern.
    
    // Anmerkung: Achten Sie darauf, wie Sie die Spezialfälle n==0 und n==1 behandeln.
}
/*** TESTCODE/MAIN ***/
int main()
{
    cout << fib(0) << endl;      // Soll 1 ausgeben.
    cout << fib(1) << endl;      // Soll 1 ausgeben.
    cout << fib(2) << endl;      // Soll 2 ausgeben.
    cout << fib(3) << endl;      // Soll 3 ausgeben.
    cout << fib(4) << endl;      // Soll 5 ausgeben.
    cout << fib(5) << endl;      // Soll 8 ausgeben.
    cout << fib(6) << endl;      // Soll 13 ausgeben.
    
    return 0;
}
