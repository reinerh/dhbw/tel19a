#include<iostream>
#include<vector>
using namespace std;

/*** AUFGABE: Suchen von Elementen in einem Vektor ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine Funktion `find()`, die als Argument einen Vektor `v` aus Zahlen und eine einzelne Zahl `x` erwartet.
Die Funktion soll die Position von `x` in `v` bestimmen und zur�ckgeben. Kommt `x` nicht vor, soll die L�nge von `v` geliefert werden.
***/
int find(vector<int> v, int x) {
    
    /* Hinweis: Verwenden Sie eine Schleife, die den Vektor vom ersten bis zum
     * letzten Element durchl�uft. Sobald Sie x finden, k�nnen Sie vorzeitig
     * abbrechen.
     */          
    return ...;
}

/*** TESTCODE/MAIN ***/
int main()
{
    vector<int> v1 = { 1, 3, 5, 5, 7, 9, 11 };
    
    cout << find(v1, 5) << endl;   // Soll '2' ausgeben.
    cout << find(v1, 1) << endl;   // Soll '0' ausgeben.
    cout << find(v1, 42) << endl;  // Soll '7' ausgeben.
}
