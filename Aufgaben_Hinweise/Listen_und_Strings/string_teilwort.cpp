#include<iostream>
#include<string>
using namespace std;

/*** AUFGABE: Suchen von Teilwörtern in einem String ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine Funktion `contains()`, die als Argumente zwei Strings s1 und
s2 erwartet. Die Funktion soll 'true' zurückliefern, falls s2 in s1 vorkommt.
Ansonsten soll sie 'false' liefern.
***/
bool contains(string s1, string s2) {
    // Sonderfall behandeln: s1 ist kürzer als s2.
   
    // Schleife durch alle Zeichen von s1, bei denen s2 noch anfangen könnte.
    // (Die Schleife muss also nicht bis zum Ende laufen)
    for (/*...*/) {
        // In jedem Durchlauf einen Hilfsstring erzeugen:
        string h;
        
        // h mit s2.size() Zeichen befüllen (in einer Schleife).
        // h soll einen Teilstring von s1 enthalten, der so lang ist wie s2.

        // h mit s2 vergleichen.
    }
    return false;
}

/*** TESTCODE/MAIN ***/
int main()
{
    cout << contains("Hallo", "Ha") << endl;      // Soll 1 ausgeben.
    cout << contains("Hallo", "Hallo") << endl;   // Soll 1 ausgeben.
    cout << contains("Hallo", "Hb") << endl;      // Soll 0 ausgeben.
    cout << contains("Ha", "Hallo") << endl;      // Soll 0 ausgeben.
    cout << contains("Hallo", "") << endl;        // Soll 1 ausgeben.
    cout << contains("", "Hallo") << endl;        // Soll 0 ausgeben.
}
