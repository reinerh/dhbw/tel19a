#include<iostream>
#include<vector>
using namespace std;

/*** AUFGABE: Suchen von Elementen in einem Vektor ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine Funktion `erase()`, die als Argument eine Vektor-Referenz v und eine einzelne positive Zahl pos erwartet.
Die Funktion soll das Element an Stelle pos aus v entfernen. Ist pos keine gültige Stelle in v, soll nichts passieren.

Anmerkung: Es muss eine Vektor-Referenz sein, damit die Operation einen Effekt hat.
***/
void erase(vector<int> & v, size_t pos) {
    // Verschieben Sie ab der Stelle pos alle Elemente jeweils um eines nach vorne.
    // Zum Schluss löschen Sie das letzte Element aus v. Dies geht z.B. mit der
    // Funktion `pop_back()`.
}

/*** HILFSFUNKTION: Gibt einen Vektor auf der Konsole aus. ***/
void print_vector(vector<int> & v) {
    for (auto el : v) {
        cout << el << " ";
    }
    cout << endl;
}

/*** TESTCODE/MAIN ***/
int main()
{
    vector<int> v1 = { 1, 3, 5, 5, 7, 9, 11 };
    erase(v1, 5);
    print_vector(v1);                          // Soll `1 3 5 5 7 11` ausgeben.
    
    vector<int> v2 = { 1, 3, 5, 5, 7, 9, 11 };
    erase(v2, 0);
    print_vector(v2);                          // Soll `3 5 5 7 9 11` ausgeben.
    
    vector<int> v3 = { 1, 3, 5 };
    erase(v3, 42);
    print_vector(v3);                          // Soll `1 3 5` ausgeben.
}
