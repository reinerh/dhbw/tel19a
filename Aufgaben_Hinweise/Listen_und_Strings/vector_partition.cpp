#include<iostream>
#include<vector>
using namespace std;

/*** AUFGABE: Aufteilen eines Vektor ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine Funktion `partition()`, die als Argumente einen Vektor `v` aus
Zahlen und eine Zahl x erwartet.
Die Funktion soll die Zahlen in v so umsortieren, dass alle Elemente links
stehen, die kleiner sind als x. Alle Elemente, die größer oder gleich x sind,
sollen rechts stehen.
***/
vector<int> partition(vector<int> v, int x) {
    vector<int> result;    
    
    /* Hinweis:
     * Verwenden Sie zwei Schleifen nacheinander, die beide v durchlaufen.
     * In der ersten Schleife hängen Sie die Elemente an 'result' an, die
     * kleiner sind als x, in der zweiten dann die restlichen Elemente.
     */
    return result;
}

/*** HILFSFUNKTION: Gibt einen Vektor auf der Konsole aus. ***/
void print_vector(vector<int> v) {
    for (auto el : v) {
        cout << el << " ";
    }
    cout << endl;
}

/*** TESTCODE/MAIN ***/
int main()
{
    print_vector(partition({1,85,6,55,3,42,77}, 42));   // Soll "1 6 3 85 55 42 77" ausgeben.
    print_vector(partition({1,2,3,4,5,6}, 4));          // Soll "1 2 3 4 5 6" ausgeben.
    print_vector(partition({1,2,3,4,5,6}, 6));          // Soll "1 2 3 4 5 6" ausgeben.
    print_vector(partition({6,5,4,3,2,1}, 4));          // Soll "3 2 1 6 5 4" ausgeben.
}
