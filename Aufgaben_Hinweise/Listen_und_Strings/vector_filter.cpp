#include<iostream>
#include<vector>
using namespace std;

/*** AUFGABE: Filtern eines Vektor ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine Funktion `filter_gerade()`, die als Argument einen Vektor `v` aus
Zahlen erwartet.
Die Funktion soll einen neuen Vektor zurückliefern, der nur die geraden Zahlen
in v enthält.
***/
vector<int> filter_gerade(vector<int> v) {
    vector<int> result;
    
    /* Hinweis:
     * Laufen Sie in einer Schleife durch v und schreiben Sie alle geraden
     * Elemente in den Ergebnis-Vektor ("result").
     * Ob eine Zahl gerade ist, finden Sie mittels des "Modulo"-Operators (%)
     * heraus.
     */
    return result;
}

/*** HILFSFUNKTION: Gibt einen Vektor auf der Konsole aus. ***/
void print_vector(vector<int> v) {
    for (auto el : v) {
        cout << el << " ";
    }
    cout << endl;
}

/*** TESTCODE/MAIN ***/
int main()
{
    print_vector(filter_gerade({1,3,4,6,3,42,77}));   // Soll "4 6 42" ausgeben.
    print_vector(filter_gerade({2,4,6,8,10}));        // Soll "2 4 6 8 10" ausgeben.
    print_vector(filter_gerade({}));                  // Soll nichts (eine leere Zeile) ausgeben.
    print_vector(filter_gerade({1,3,5}));   // Soll nichts (eine leere Zeile) ausgeben.
}
