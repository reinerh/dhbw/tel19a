# Übungsaufgaben

Dieser Ordner enthält Übungsaufgaben zu `C++`. Jede Aufgabe steht in einer eigenen Quelldatei und kann direkt dort bearbeitet werden. In den Dateien finden sich jeweils die Aufgabenstellungen als Kommentar sowie eine Main-Funktion, in der die zu schreibenden Funktionen benutzt werden. Dort steht als Kommentar daneben, was für eine Ausgabe wir jeweils erwarten.