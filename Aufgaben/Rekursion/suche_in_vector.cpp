#include<iostream>
#include<vector>
using namespace std;

/*** AUFGABE: Suchen von Teilwörtern in einem String ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine rekursive Funktion `find()`, die als Argumente einen `vector<int>` und eine Zahl n erwartet.
Die Funktion soll n im Vektor suchen und ihre Position zurückliefern. Ist n nicht enthalten, soll die Länge des
Vektor zurückgegeben werden.
***/
int find(vector<int> v, int n) {

}

/*** TESTCODE/MAIN ***/
int main()
{
    cout << find({1,3,5,7,9},3)  << endl;      // Soll 1 ausgeben.
    cout << find({1,3,5,7,9},5)  << endl;      // Soll 2 ausgeben.
    cout << find({1,3,5,7,9},42)  << endl;     // Soll 5 ausgeben.
    cout << find({},3)  << endl;               // Soll 0 ausgeben.
    
    return 0;
}
