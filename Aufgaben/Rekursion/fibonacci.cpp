#include<iostream>
using namespace std;

/*** AUFGABE: Suchen von Teilwörtern in einem String ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine rekursive Funktion `fib()`, die als Argument eine positive Zahl `n` erwartet.
Die Funktion soll die n-te Fibonacci-Zahl zurückliefern.
***/
unsigned int fib(unsigned int n) {

}

/*** TESTCODE/MAIN ***/
int main()
{
    cout << fib(0) << endl;      // Soll 1 ausgeben.
    cout << fib(1) << endl;      // Soll 1 ausgeben.
    cout << fib(2) << endl;      // Soll 2 ausgeben.
    cout << fib(3) << endl;      // Soll 3 ausgeben.
    cout << fib(4) << endl;      // Soll 5 ausgeben.
    cout << fib(5) << endl;      // Soll 8 ausgeben.
    cout << fib(6) << endl;      // Soll 13 ausgeben.
    
    return 0;
}
