#ifndef UI_H
#define UI_H

#include "common.h"
#include<string>
/*** Anmerkung: 
     Da dies eine Header-Datei ist, lassen wir die sonst uebliche Direktive
     'using namespace std;' weg. Mit dieser würden wir das 'using' sonst jedem
     aufzwingen, der diesen Header einbindet.
    
    Gundsaetzliche Regel: Niemals 'using namespace' in Headerdateien nutzen.
***/

/** Liefert die String-Repraesentation des angegebenen Zugs zurueck. **/
std::string to_string(Move m);

/** Liest einen Spielzug vom Spieler ein und liefert diesen als 'Moves' zurueck.
    'player_name' ist dabei der Name des Spielers, der einen Zug machen soll.
**/
Move get_move(std::string player_name);

/** Bildschirmausgabe fuer den Fall, dass ein Spieler gewonnen hat.
    'player_name' ist dabei der Name des Spielers, der gewonnen hat.
**/
void print_winner(std::string player_name);

/** Bildschirmausgabe fuer den Fall, dass das Spiel unentschieden endet.
**/
void print_draw();

#endif