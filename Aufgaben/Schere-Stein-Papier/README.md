# Aufgabe: Schere-Stein-Papier

Implementieren Sie das Spiel "Schere-Stein-Papier" auf ähnliche Weise, wie wir in der Vorlesung TicTacToe behandelt haben.

In diesem Verzeichnis sind einige Quelldateien vorgegeben, die Sie als Programmgerüst benutzen können.
Der Code ist dabei auf mehrere Quelldateien verteilt:

`main.cpp`:
Enthält Code, der das eigentliche Spiel startet.

`ui.cpp`, `ui.h`:
Enthält Spiel-Code für die Benutzerinteraktion.

`logic.cpp`, `logic.h`:
Enthält die eigentliche Programmlogik des Spiels.

`common.h`:
Enthält gemeinsame Definitionen fuer UI und Logik.
