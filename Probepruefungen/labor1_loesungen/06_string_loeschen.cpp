#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Strings, 6 Punkte ***/

/*** AUFGABENSTELLUNG:
     Schreiben Sie eine Funktion 'loeschen', die als Argumente einen
     String s und einen Buchstaben c erwartet.
     Die Funktion soll jedes Vorkommen von c in s löschen und das
     Ergebnis zurückliefern.
***/
string loeschen(string s, char c);

/*** TESTCODE/MAIN: ***/
int main()
{
    cout << loeschen("Hallo", 'a') << endl;       // Soll "Hllo" ausgeben.
    cout << loeschen("Hallo", 'l') << endl;       // Soll "Hao" ausgeben.
    cout << loeschen("Hallo", 'x') << endl;       // Soll "Hallo" ausgeben.
    
    return 0;
}

/*** LOESUNG: ***/
string loeschen(string s, char c)
{
    for (int i=0; i<s.size(); i++)
    {
        if (s[i] == c)
        {
            for (int j=i+1; j<s.size(); j++)
            {
                s[j-1] = s[j];
            }
            s.pop_back();
            i--;
        }
    }
    return s;
}
