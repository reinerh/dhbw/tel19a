#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Arrays, 6 Punkte ***/

/*** AUFGABENSTELLUNG:
     Schreiben Sie eine Funktion, die als Parameter eine int Zahl n erwartet.
     
    Die Funktion soll einen zweidimensionalen int-Vektor mit n mal n Feldern zur�ckgeben,
    der jeweils in Zeile i die ersten 10 Vielfachen von i enth�lt.
***/
vector<vector<int>> einmaleins(int n);

/*** HILFSFUNKTIONEN: ***/
void print_vector(vector<vector<int>> v)
{
    for (int i=0; i<v.size(); i++)
    {
        for (int j=0; j<v[i].size(); j++)
            cout << v[i][j] << " ";
        cout << endl;
    }
}

/*** TESTCODE/MAIN: ***/
int main()
{
    vector<vector<int>> v = einmaleins(10);
    
    print_vector(v);
    /* So soll die Ausgabe aussehen:
    1 2 3 4 5 6 7 8 9 10
    2 4 6 8 10 12 14 16 18 20
    3 6 9 12 15 18 21 24 27 30
    4 8 12 16 20 24 28 32 36 40
    ... 
    */
    return 0;
}


/*** LOESUNG: ***/
vector<vector<int>> einmaleins(int n)
{
    vector<vector<int>> ergebnis;
    for (int i=0; i<n; i++)
    {
        vector<int> zeile;
        for (int j=0; j<n; j++)
            zeile.push_back((i+1)*(j+1));
        ergebnis.push_back(zeile);
    }
    return ergebnis;
}
