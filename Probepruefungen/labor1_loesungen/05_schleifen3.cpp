#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Schleifen, 4 Punkte ***/

/*** AUFGABENSTELLUNG:
     Schreiben Sie eine Funktion, die als Parameter zwei Buchstaben z1 und z2 erwartet.
     
     Die Funktion soll alle Buchstaben im Alphabet auf der Konsole ausgeben,
     die zwischen z1 und z2 liegen.
***/ 
void buchstaben(char z1, char z2);

/*** TESTCODE/MAIN: ***/
int main()
{
    buchstaben('a','c'); // Soll 'b' ausgeben
    buchstaben('a','z'); // Soll das gesamte kleine Alphabet (ohne 'a' und 'z') ausgeben
    return 0;
}

/*** LOESUNG: ***/
void buchstaben(char z1, char z2)
{
    for (char z = z1+1; z<z2; z++)
        cout << z << endl;
}
