#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Arrays, 4 Punkte ***/

/*** AUFGABENSTELLUNG:
    Schreiben Sie eine Funktion multeven, die eine Liste von Zahlen erwartet.
    
    Die Funktion soll das Produkt der Elemente an geraden Positionen zurückliefern.
    Die Position 0 ist gerade.
***/
int multeven(vector<int> liste);


/*** TESTCODE/MAIN ***/
int main() {
    
    vector<int> v1 = {1,2,3,4,5};     // Produkt: 1*3*5 = 15
    cout << multeven(v1) << endl;      // Soll 15 ausgeben
    
    vector<int> v2 = {5,4,3,2,1};     // Produkt: 5*3*1 = 15
    cout << multeven(v2) << endl;      // Soll 15 ausgeben
    
    vector<int> v3 = {3,29,0,42,2};   // Produkt: 3*0*2 = 0
    cout << multeven(v3) << endl;      // Soll 0 ausgeben
    
    vector<int> v4;                   // Produkt: 1
    cout << multeven(v4) << endl;      // Soll 1 ausgeben
    return 0;
}


