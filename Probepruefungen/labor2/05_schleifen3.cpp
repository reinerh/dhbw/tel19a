#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Schleifen, 4 Punkte ***/

/*** AUFGABENSTELLUNG:
     Schreiben Sie eine Funktion, die als Parameter zwei Buchstaben z1 und z2 erwartet.
     
     Die Funktion soll alle Buchstaben im Alphabet auf der Konsole ausgeben,
     die nicht zwischen z1 und z2 liegen.
***/ 
void buchstaben(char z1, char z2);

/*** TESTCODE/MAIN: ***/
int main()
{
    buchstaben('a','c'); // Soll das gesamte Alphabet ohne 'b' ausgeben.
    buchstaben('a','z'); // 'a' und 'z' ausgeben.
    return 0;
}

