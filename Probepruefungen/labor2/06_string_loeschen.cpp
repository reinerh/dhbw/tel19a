#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Strings, 6 Punkte ***/

/*** AUFGABENSTELLUNG:
     Schreiben Sie eine Funktion 'loeschen', die als Argumente einen
     String s und einen Buchstaben c erwartet.
     Die Funktion soll jedes Vorkommen von c in s löschen, wenn c
     mehr als einmal in s vorkommt. Dann soll die Funktion das
     Ergebnis zurückliefern.
***/
string loeschen(string s, char c);

/*** TESTCODE/MAIN: ***/
int main()
{
    cout << loeschen("Hallo", 'a') << endl;       // Soll "Hallo" ausgeben (a kommt nur einmal vor).
    cout << loeschen("Hallo", 'l') << endl;       // Soll "Hao" ausgeben (l kommt zweimal vor).
    cout << loeschen("Hallo", 'x') << endl;       // Soll "Hallo" ausgeben.
    
    return 0;
}

