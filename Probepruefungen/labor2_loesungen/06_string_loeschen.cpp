#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Strings, 6 Punkte ***/

/*** AUFGABENSTELLUNG:
     Schreiben Sie eine Funktion 'loeschen', die als Argumente einen
     String s und einen Buchstaben c erwartet.
     Die Funktion soll jedes Vorkommen von c in s löschen, wenn c
     mehr als einmal in s vorkommt. Dann soll die Funktion das
     Ergebnis zurückliefern.
***/
string loeschen(string s, char c);

/*** TESTCODE/MAIN: ***/
int main()
{
    cout << loeschen("Hallo", 'a') << endl;       // Soll "Hallo" ausgeben (a kommt nur einmal vor).
    cout << loeschen("Hallo", 'l') << endl;       // Soll "Hao" ausgeben (l kommt zweimal vor).
    cout << loeschen("Hallo", 'x') << endl;       // Soll "Hallo" ausgeben.

    return 0;
}

/*** LOESUNG: ***/
// Hilfsfunktion count(string s, char c): Zaehlt, wie oft c in s vorkommt.
int count(string s, char c)
{
    int res = 0;
    for (auto el : s) {
        if (el == c) {
            res++;
        }
    }
    return res;
}

string loeschen(string s, char c)
{
    if (count(s,c) <= 1) { return s; }
    for (int i=0; i<s.size(); i++)
    {
        if (s[i] == c)
        {
            for (int j=i+1; j<s.size(); j++)
            {
                s[j-1] = s[j];
            }
            s.pop_back();
            i--;
        }
    }
    return s;
}
