#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Arrays, 6 Punkte ***/

/*** AUFGABENSTELLUNG:
     Schreiben Sie eine Funktion, die als Parameter eine int Zahl n erwartet.
     
    Die Funktion soll ein zweidimensionalen int-Vektor mit n mal n Feldern zur�ckgeben,
    der in aufsteigendern Reihenfolge die Zahlen von 1 bis n*n enth�lt.
***/
vector<vector<int>> zahlen(int n);


/*** TESTCODE/MAIN: ***/
int main()
{
    vector<vector<int>> v = zahlen(10);
    
    // Der folgende Code gibt das Array zeilenweise aus:
    for (int i=0; i<10; i++)
    {
        for (int j=0; j<10; j++)
            cout << v[i][j] << " ";
        cout << endl;
    }
    /* So soll die Ausgabe aussehen:
    1 2 3 4 5 6 7 8 9 10
    11 12 13 14 15 16 17 18 19 20
    21 22 23 24 25 26 27 28 29 30
    ... 
    91 92 93 94 95 96 97 98 99 100
    */
    return 0;
}


/*** LOESUNG: ***/
vector<vector<int>> zahlen(int n)
{
    vector<vector<int>> ergebnis;
    for (int i=0; i<10; i++)
    {
        vector<int> zeile;
        for (int j=0; j<10; j++)
            zeile.push_back(i*n+j+1);
        ergebnis.push_back(zeile);
    }
    return ergebnis;
}
