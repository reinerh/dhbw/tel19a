#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Schleifen, 4 Punkte ***/

/*** AUFGABENSTELLUNG:
     
     Schreiben Sie eine Funktion bar, die den Effekt der Funktion foo (s.u.)
     umkehrt. 
     
     D.h. es soll f�r jede Eingabe bar(foo(x)) == x  gelten.
***/
int bar(int x);


/*** VORGABE: ***/
int foo(int x)
{
    for (int i=2; i<10; i++) {
        x += 2 * i;
    }
    return x;
}

/*** TESTCODE/MAIN: ***/
int main() {
    
    printf("%d\n",bar(foo(1)));           // Soll 1 ausgeben
    printf("%d\n",bar(foo(42)));          // Soll 42 ausgeben
    printf("%d\n",bar(foo(-2)));          // Soll -2 ausgeben
      
    return 0;
}

/*** LOESUNG: ***/
int bar(int x)
{                                         // 1 Punkt, wenn es compiliert
    for (int i=9; i>1; i--) {             // 2 Punkte f�r Schleife mit richtiger Bedingung/Z�hlweise
        x -= 2 * i;                           // 1 Punkt f�r Berechnung
    }
    return x;
}
