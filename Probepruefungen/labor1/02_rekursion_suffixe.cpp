#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Rekursion, 6 Punkte ***/

/*** AUFGABENSTELLUNG:
     Schreiben Sie eine rekursive Funktion 'suffixe', die einen
     String s als Argument erwartet.
     Die Funktion soll alle Wortenden von s in einem Vektor liefern.
***/
vector<string> suffixe(string s);

/*** TESTCODE/MAIN: ***/
int main()
{
    for (string p : suffixe("ab"))
        cout << p << " ";                       // Soll "b ab" ausgeben.
    cout << endl;
    
    for (string p : suffixe("abc"))
        cout << p << " ";                       // Soll "c bc abc" ausgeben.
    cout << endl;
    
    for (string p : suffixe("Hallo"))
        cout << p << " ";                       // Soll "o lo llo allo Hallo" ausgeben.
    cout << endl;
    
    return 0;
}

