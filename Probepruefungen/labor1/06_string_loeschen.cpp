#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Strings, 6 Punkte ***/

/*** AUFGABENSTELLUNG:
     Schreiben Sie eine Funktion 'loeschen', die als Argumente einen
     String s und einen Buchstaben c erwartet.
     Die Funktion soll jedes Vorkommen von c in s löschen und das
     Ergebnis zurückliefern.
***/
string loeschen(string s, char c);

/*** TESTCODE/MAIN: ***/
int main()
{
    cout << loeschen("Hallo", 'a') << endl;       // Soll "Hllo" ausgeben.
    cout << loeschen("Hallo", 'l') << endl;       // Soll "Hao" ausgeben.
    cout << loeschen("Hallo", 'x') << endl;       // Soll "Hallo" ausgeben.
    
    return 0;
}

