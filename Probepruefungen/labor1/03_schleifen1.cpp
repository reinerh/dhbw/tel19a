#include<iostream>
#include<vector>
#include<string>
using namespace std;

/*** AUFGABE: Schleifen, 6 Punkte ***/

/*** AUFGABENSTELLUNG:
   
   Schreiben Sie eine Funktion, die als Parameter eine Zahl n erwartet.
   Die Funktion soll alle Quadratzahlen auf der Konsole ausgeben, die kleiner als n sind.
***/
void quadratzahlen(int n);

/*** TESTCODE/MAIN: ***/
int main()
{
    quadratzahlen(10);   // Soll 0 1 4 9 ausgeben
    quadratzahlen(100);  // Soll 0 1 4 9 16 25 36 49 64 81 ausgeben
    return 0;
}

