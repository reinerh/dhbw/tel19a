#include<iostream>
#include<string>
#include<ctime>
using namespace std;

// Liefert die Sekunden, die am heutigen Tag vergangen sind.
int sekunden_tag(time_t t) {
	// Ein ganzer Tag hat 86400 Sekunden.
	// Diese so oft abziehen, bis der Wert kleiner ist als 86400.
	// Oder einfach modulo 86400 rechnen.	
    return t % 86400;
}

// Liefert die Stunden am heutigen Tag.
int get_hour(int t) {
	return ((t / 3600) + 1) % 24;
}

// Liefert die Minuten in der aktuellen Stunde.
int get_minute(int t) {
	//return (t / 60) % 60;
	return t % 3600 / 60;
}

// Liefert die Sekunden in der aktuellen Minute.
int get_second(int t) {
	return t % 60;
}

string to_bin(int t) {
	string binzahl = "      ";
	int i=5;
	while (t!=0) {
		// Wenn t nicht durch 2 teilbar ist,
		// dann '*' einfügen, sonst ' '.
		if (t%2 == 1) {
			binzahl[i] = '*';
		}
		else {
			binzahl[i] = ' ';
		}
		t = t/2;
		i--;
	}
	
	return binzahl;
}

// Liefert n als Binärstring zurück.
string to_bin2(int t) {
	string binzahl = "      ";
	for (int i = 0; i<6; i++) {
		if (t!=1) {
			// Wenn t nicht durch 2 teilbar ist,
			// dann '*' einfügen, sonst ' '.
			if (t%2 == 1) {
				binzahl[i] = '*';
			}
			else {
				binzahl[i] = ' ';
			}
			t = t/2;
		}else{
			binzahl[i] = '*';
			break;
		}
	}
	return binzahl;
}

int main() {
	
	time_t t = time(nullptr);
	cout << to_bin(get_hour(sekunden_tag(t))) << endl;
	cout << to_bin(get_minute(sekunden_tag(t))) << endl;
	cout << to_bin(get_second(sekunden_tag(t))) << endl;
	
	return 0;
}