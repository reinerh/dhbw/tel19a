#include<iostream>
#include<string>
using namespace std;


void print_spielfeld(string spielfeld) {
	
	for (int i=0; i<9; i++) {
		cout << " " << spielfeld[i] << " ";
		if (i==2 || i==5) {
			cout << endl;
			cout << "---+---+---" << endl;
		}
		else if (i!=8) {
			cout << "|";
		}
	}
	cout << endl;
}


void eingabe(string & spielfeld, char spieler) {
	// Spielfeld ausgeben
	print_spielfeld(spielfeld);
	
	// Spieler nach einer Eingabe fragen und diese einlesen.
	cout << "Bitte waehlen Sie ein Feld: ";
	int eingabe;
	cin >> eingabe;
	
	// TODO: Eingabe pruefen und ggf. korrigieren!
	// An dieser Stelle sollte 0<=eingabe<=8 sein.

	// Eingabe ins Spielfeld eintragen.
	spielfeld[eingabe] = spieler;
}


void run() {
	// Spieler nach Namen fragen
	
	// Spielfeld erzeugen
	string spielfeld = "123456789";
	
	// Festlegen, welcher Spieler am Zug ist
	char spieler = 'X';
	
	// Solange das Spiel noch läuft
	while (true) {  // TODO: true durch einen echten Test ersetzen.
		// Spieler macht einen Zug.
		eingabe(spielfeld, spieler);
		// Spieler wechseln.
		if (spieler == 'X') { spieler = 'O'; }
		else { spieler = 'X'; }
	}
	
}

int main() {
	
	print_spielfeld("123456789");	
	
	run();
	
	return 0;
}