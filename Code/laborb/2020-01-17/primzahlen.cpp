#include<iostream>
#include<vector>
using namespace std;


// Liefert true, wenn 'zahl' eine Primzahl ist.
// Sonst false.
bool prim(int zahl) {
	if (zahl < 2) {
		return false;
	}
	
	for (int i=2; i<zahl; i++) {
		if (zahl % i == 0) {
			return false;
		}
	}
	return true;
}

int prim2(int zahl, int i=2) {
	if (zahl < 2) return false;
	if (i >= zahl) return true;
	
	return (zahl % i != 0) && prim2(zahl, i+1);	
}


template<typename T>
void print_vector(vector<T> v) {
	for (T e : v) {
		cout << e << " ";
	}
	cout << endl;
}


vector<int> primzahlen(int n) {
	vector<int> a;
	for (int i=1; i<=n; i++) {
		if (prim(i)) {
			a.push_back(i);
		}
	}
	return a;
}


vector<int> primfaktoren(int n) {
	vector<int> ergebnis;
	int i = 2;
	while (n != 1) {
		while (n % i == 0) {
			ergebnis.push_back(i);
			n /= i;  // n = n / i;
		}
		i++;
	}
	return ergebnis;
}



int main() {

	cout << prim2(5) << endl;
	cout << prim2(7) << endl;
	cout << prim2(13) << endl;
	cout << prim2(12) << endl;
	cout << prim2(60) << endl;
	cout << prim2(1) << endl;
	
	print_vector(primzahlen(100));

	print_vector(primfaktoren(60));
	
	return 0;
}