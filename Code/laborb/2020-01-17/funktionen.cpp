#include<iostream>
using namespace std;

int summe_bis(int x) {
	int ergebnis = 0;
	
	for (int i=1; i<=x; i=i+1) {
		ergebnis = ergebnis + i;
	}
	
	return ergebnis;
}

int main() {
	
	int eingabe;
	cout << "Bitte eine Zahl eingeben: ";
	cin >> eingabe;
	cout << "Die Summe der Zahlen von 1 bis " << eingabe
	     << " ist " << summe_bis(eingabe) << endl;

	
	return 0;
}