#include"woerterbuch.h"
using namespace std;

void Woerterbuch::load(string dateiname)
{
    // TODO
}

void Woerterbuch::add(string de, string en)
{
	Eintrag e;
	e.de = de;
	e.en = en;

	data.push_back(e);
}

string Woerterbuch::suche_en(string de)
{
	for (Eintrag e : data)
	{
		if (e.de == de) { return e.en; }
	}

	return "";
}
