#include<iostream>
#include<string>
using namespace std;


void print_spielfeld(string spielfeld) {
	
	for (int i=0; i<9; i++) {
		cout << " " << spielfeld[i] << " ";
		if (i==2 || i==5) {
			cout << endl;
			cout << "---+---+---" << endl;
		}
		else if (i!=8) {
			cout << "|";
		}
	}
	cout << endl;
}


void eingabe(string & spielfeld, char spieler) {
	// Spielfeld ausgeben
	print_spielfeld(spielfeld);
	
	// Spieler nach einer Eingabe fragen und diese einlesen.
	cout << "Bitte waehlen Sie ein Feld: ";
	int input;
	cin >> input;
	
	// TODO: Eingabe pruefen und ggf. korrigieren!
	// An dieser Stelle sollte 0<=eingabe<=8 sein.
	input--;
	if (input >= 0 && input <= 8 && 
	    spielfeld[input] != 'X' && 
		spielfeld[input] != 'O') {
		// Eingabe ins Spielfeld eintragen.
		spielfeld[input] = spieler;		
	} else {
		cout << "Die Eingabe war ungueltig." << endl;
		eingabe(spielfeld, spieler);
	}
}

bool zeile_voll(string spielfeld, char spieler) {
}

bool spalte_voll(int i, string spielfeld, char spieler) {
	return spielfeld[i] == spieler &&
	       spielfeld[i+3] == spieler &&
		   spielfeld[i+6] == spieler;
}

bool spalte_voll(string spielfeld, char spieler) {
	return spalte_voll(0, spielfeld, spieler) ||
		   spalte_voll(1, spielfeld, spieler) ||
		   spalte_voll(2, spielfeld, spieler)
}

bool diag_voll(string spielfeld, char spieler) {
}

bool gewonnen(string spielfeld, char spieler) {
	return zeile_voll(spielfeld, spieler) ||
	       spalte_voll(spielfeld, spieler) ||
		   diag_voll(spielfeld, spieler);
}

void run() {
	// Spieler nach Namen fragen
	
	// Spielfeld erzeugen
	string spielfeld = "123456789";
	
	// Festlegen, welcher Spieler am Zug ist
	char spieler = 'X';
	
	// Solange das Spiel noch läuft
	while (!gewonnen(spielfeld, 'X') &&
	       !gewonnen(spielfeld, 'O') &&
		   !unentschieden()) {  // TODO: true durch einen echten Test ersetzen.
		// Spieler macht einen Zug.
		eingabe(spielfeld, spieler);
		// Spieler wechseln.
		if (spieler == 'X') { spieler = 'O'; }
		else { spieler = 'X'; }
	}
	
}

int main() {
	
	print_spielfeld("123456789");	
	
	run();
	
	return 0;
}