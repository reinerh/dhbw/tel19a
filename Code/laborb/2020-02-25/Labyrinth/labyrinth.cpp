#include"labyrinth.h"

#include<fstream>
#include<sstream>
using namespace std;

Labyrinth::Labyrinth(std::string filename)
{
	ifstream file(filename);
	string line;
	
	while (getline(file, line))
	{
		feld.push_back(line);
	}
	file.close();
	setPlayerPos();
	direction=0;
}

void Labyrinth::setPlayerPos()
{
	for (int zeile=0; zeile<feld.size(); zeile++)
	{
		for (int spalte=0; spalte<feld[zeile].size(); spalte++)
		{
		  if (feld[zeile][spalte] == 'S')
		  {
		  	py = zeile;
		  	px = spalte;
		  }
		}
	}
}

std::string Labyrinth::str() const
{
	ostringstream out;
	for (auto line : feld)
	{
		out << line << endl;
	}
	return out.str();
}

// Prueft, ob der Spieler sich an die angegebene Position begeben darf.
bool Labyrinth::move_allowed(int x, int y)
{
	return (x>=0 && y >=0 && y<feld.size() && x<feld[y].size() && feld[y][x] != '#');
}

// Prueft, ob der Spieler sich nach oben bewegen darf.
bool Labyrinth::move_forward_allowed()
{
	switch(direction)
  	{
  		case 0: return move_allowed(px,py-1); // oben
  		case 1: return move_allowed(px+1,py); // rechts
  		case 2: return move_allowed(px,py+1); // unten
  		case 3: return move_allowed(px-1,py); // links
	}
}

bool Labyrinth::move_left_allowed()
{
	return move_allowed(px-1, py);
}

bool Labyrinth::move_backward_allowed()
{
	return move_allowed(px, py+1);
}

bool Labyrinth::move_right_allowed()
{
	return move_allowed(px+1, py);
}

void Labyrinth::move_forward()
{
  if (move_forward_allowed())
  {
  	switch(direction)
  	{
  		case 0: py--; break; // oben
  		case 1: px++; break; // rechts
  		case 2: py++; break; // unten
  		case 3: px--; break; // links
	}
  	feld[py][px] = 'S';
  	direction = 0;
  }
}

void Labyrinth::move_right()
{
  if (move_right_allowed())
  {
  	switch(direction)
  	{
  		case 0: px++; break; // oben
  		case 1: py++; break; // rechts
  		case 2: px--; break; // unten
  		case 3: py--; break; // links
	}
  	feld[py][px] = 'S';
  	direction = 1;
  }
}

void Labyrinth::move_backward()
{
  if (move_backward_allowed())
  {
  	switch(direction)
  	{
  		case 0: py++; break; // oben
  		case 1: px--; break; // rechts
  		case 2: py--; break; // unten
  		case 3: px++; break; // links
	}
  	feld[py][px] = 'S';
  	direction = 2;
  }
}

void Labyrinth::move_left()
{
  if (move_left_allowed())
  {
  	switch(direction)
  	{
  		case 0: px--; break; // oben
  		case 1: py--; break; // rechts
  		case 2: px++; break; // unten
  		case 3: py++; break; // links
	}
  	feld[py][px] = 'S';
  	direction = 3;
  }
}

ostream & operator<<(ostream & lhs, Labyrinth const & rhs)
{
	return lhs << rhs.str();
}

