#ifndef LABYRINTH_H
#define LABYRINTH_H

#include<string>
#include<vector>
#include<iostream>

struct Labyrinth
{
	std::vector<std::string> feld;
	int px;
	int py;
	int direction;  // 0: oben, 1: rechts, 2: unten, 3: links
	
	Labyrinth(std::string filename);
	
	void setPlayerPos();
	std::string str() const;
	
	bool move_allowed(int x, int y);
	
	bool move_forward_allowed();
	bool move_right_allowed();
	bool move_backward_allowed();
	bool move_left_allowed();
	
	void move_forward();
	void move_right();
	void move_backward();
	void move_left();
};

std::ostream & operator<<(std::ostream & lhs, Labyrinth const &);

#endif

