#include"woerterbuch.h"

#include<fstream>
#include<sstream>
using namespace std;

void Woerterbuch::load(string dateiname)
{
    ifstream file(dateiname);
    
    string line;
    while(getline(file,line))
    {
    	stringstream buffer;
    	buffer << line;
    	
	    string de;
	    string en;
	    
    	if(buffer >> de)
    	{
		  	getline(buffer,en);
  			add(de,en);
		}
	}
}

void Woerterbuch::add(string de, string en)
{
	Eintrag e;
	e.de = de;
	e.en = en;

	data.push_back(e);
}

string Woerterbuch::suche_en(string de)
{
	for (Eintrag e : data)
	{
		if (e.de == de) { return e.en; }
	}

	return "";
}
