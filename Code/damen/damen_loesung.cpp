#include<iostream>
#include<vector>
#include<string>
using namespace std;

void print_spielfeld(vector<string> const & spielfeld);

bool loese_spalte(vector<string> & spielfeld, int spalte);

bool dame_erlaubt(vector<string> const & spielfeld, int zeile, int spalte);


int main() {
	
	//Spielfeld erzeugen
	vector<string> spielfeld(8,"        ");
	loese_spalte(spielfeld, 0);
	print_spielfeld(spielfeld);
	return 0;
}

void print_spielfeld(vector<string> const & spielfeld) {
	cout << "**********" << endl;
	for (auto line : spielfeld) {
		cout << '*' << line << '*' << endl;
	}
	cout << "**********" << endl << endl;
}

bool loese_spalte(vector<string> & spielfeld, int spalte) {
	// Wenn spalte == 8 ist, dann ist nichts mehr zu tun.
	if (spalte == 8) {
		return true;
	}
	
	// Versuche, von oben bis unten in der gegebenen Spalte
	// eine Dame zu setzen.
	for (int zeile=0; zeile < 8; zeile++) {
		// Wenn in spielfeld[zeile][spalte] eine Dame gesetzt werden kann...
		if (dame_erlaubt(spielfeld, zeile, spalte)) {
			// ... dann setzen wir die Dame.
			spielfeld[zeile][spalte] = 'X';

			// Wenn das Spiel jetzt für die nächste Spalte gelöst werden kann,
			// dann sind wir fertig
			if (loese_spalte(spielfeld, spalte+1)) {
				return true;
			}
			// sonst entfernen wir die Dame wieder aus der Zeile.
			else {
				spielfeld[zeile][spalte] = ' ';
			}
		}
	}
	
	return false;
}

bool dame_erlaubt(vector<string> const & spielfeld, int zeile, int spalte) {
	// Generell prüfen wir nur die Felder links von der gegebenen
	// Position, da aufgrund unseres Gesamt-Algorithmus die Felder
	// rechts nicht belegt sein können.
	
	// Zeile pruefen
	for (int x=0; x<spalte; x++)
	{
		if (spielfeld[zeile][x] == 'X') { return false; }
	}
	
	// Diagonale von links oben nach rechts unten pruefen.
	//   1. Anfangspunkt bestimmen
	//      Dieser kann auch ausserhalb des Spielfelds liegen.
	//      
	int x = 0;
	int y = zeile - spalte;
	//   2. Diagonale entlang laufen. Dabei pruefen, ob der
	//      Punkt im Feld ist und ob er belegt ist.
    while (x<spalte && y<zeile)
	{
		if (y>=0 && spielfeld[y][x] == 'X') { return false; }
		x++;
		y++;
	}
	
	// Analog zu oben die andere Diagonale pruefen.
	x = 0;
	y = zeile + spalte;
	while (x<spalte && y<zeile)
	{
		if (y<=7 && spielfeld[y][x] == 'X') { return false; }
		x++;
		y--;
	}
	
	return true;
}




