#include<iostream>
#include<vector>
#include<string>
using namespace std;

void print_spielfeld(vector<string> const & spielfeld);

bool loese_spalte(vector<string> & spielfeld, int spalte);

bool dame_erlaubt(vector<string> const & spielfeld, int zeile, int spalte);


int main() {
	
	//Spielfeld erzeugen 
	//TODO: Initialisierung!
	vector<string> spielfeld;
	
	loese_spalte(spielfeld, 0);
	
	print_spielfeld(spielfeld);
	
	
	return 0;
}

void print_spielfeld(vector<string> const & spielfeld) {
	// TODO: Implementieren
}

bool loese_spalte(vector<string> & spielfeld, int spalte) {
	// Wenn spalte == 8 ist, dann ist nichts mehr zu tun.
	if (spalte == 8) {
		return true;
	}
	
	// Versuche, von oben bis unten in der gegebenen Spalte
	// eine Dame zu setzen.
	for (int zeile=0; zeile < 8; zeile++) {
		// Wenn in spielfeld[zeile][spalte] eine Dame gesetzt werden kann...
		if (dame_erlaubt(spielfeld, zeile, spalte)) {
			// ... dann setzen wir die Dame.
			/*...*/

			// Wenn das Spiel jetzt für die nächste Spalte gelöst werden kann,
			// dann sind wir fertig
			if (loese_spalte(spielfeld, spalte+1)) {
				return true;
			}
			// sonst entfernen wir die Dame wieder aus der Zeile.
			else {
				/*...*/
			}
		}
	}
	
	return false;
}

bool dame_erlaubt(vector<string> const & spielfeld, int zeile, int spalte) {
	// TODO: Implementieren
	return false;
}




