#include<iostream>
#include<string>
#include<vector>
using namespace std;

void print_spielfeld(vector<string> spielfeld);

// Liest eine Position vom Spieler und trägt an der gegeben Stelle
// das Zeichen s im Spielfeld ein.
void eingabe(vector<string> & spielfeld, char s) {
	// Spielfeld anzeigen
	print_spielfeld(spielfeld);
	
	// Spieler nach seiner Eingabe fragen
	cout << "Bitte ein Feld waehlen: ";
	int input;
	cin >> input;
	input--;
	int zeile = input / 3;
	int spalte = input % 3;
	
	// Eingabe prüfen
	if (input >= 0 && input <= 8) {
		// Spielzug eintragen.
		spielfeld[zeile][spalte] = s;		
	} else {
		cout << "Das war nix" << endl;
		eingabe(spielfeld, s);
	}
}

// Gibt das Spielfeld auf die Konsole aus
void print_spielfeld(vector<string> spielfeld) {
	for (int i = 0; i<spielfeld.size(); i++) {
		string zeile = spielfeld[i];
		cout << " " << zeile[0] 
		     << " | " << zeile[1]
			 << " | " << zeile[2]
			 << endl;
		if (i<2) {
			cout << "---+---+---" << endl;
		}
	}
}

void run() {
	// Spielfeld erzeugen
	vector<string> spielfeld = { "123", "456", "789" };
	char s = 'X';
	
	while (true/* Spiel nicht zu Ende */) {
		// Spieler macht seinen Zug
		eingabe(spielfeld, s);
		
		// Spieler wechseln
		if (s=='X') { s = 'O'; }
		else { s = 'X'; }
		
		// Alternative Art, den Spieler zu wechseln:
		//s = (s=='X' ? 'O' : 'X');
		
	}
	// Auswerten und Gewinner anzeigen.
}

int foo() {
	return 42;
}


int main() {
	run();
	
	return 0;
}