#include"woerterbuch.h"

#include<iostream>
using namespace std;

int main() {
	Woerterbuch w;

	w.load("de_en.txt");
	//w.add("Haus", "house");
	
	auto w1 = w.suche_en("Foo");
	for (string word : w1)
	{
		cout << word << endl; // Soll "house" ausgeben
	}

	return 0;
}
