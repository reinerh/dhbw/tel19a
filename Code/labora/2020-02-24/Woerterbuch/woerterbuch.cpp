#include"woerterbuch.h"

#include<fstream>
#include<sstream>
using namespace std;

void Woerterbuch::load(string dateiname)
{
	ifstream file(dateiname);
	
	string line;
	
	while(getline(file, line))
	{
		// Eine Zeile in einen Stringstream s puffern.
		stringstream linebuffer;
		linebuffer << line;
		
		// Jedes Wort aus der Zeile einzeln aus s holen.
		vector<string> en;
		
		string de;
		if (linebuffer >> de)
		{
			string h;
			while(linebuffer >> h)
			{
				en.push_back(h);
			}
			add(de,en);
		}
	}
	file.close();
}

void Woerterbuch::add(string de, vector<string> en)
{
	Eintrag e;
	e.de = de;
	e.en = en;

	data.push_back(e);
}

vector<string> Woerterbuch::suche_en(string de)
{
	for (Eintrag e : data)
	{
		if (e.de == de) { return e.en; }
	}

	return {};
}
