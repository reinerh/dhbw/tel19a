#ifndef WOERTERBUCH_H
#define WOERTERBUCH_H

#include<string>
#include<vector>

struct Eintrag {
	std::string de;
	std::vector<std::string> en;
};

struct Woerterbuch {
	std::vector<Eintrag> data;

	void load(std::string dateiname);
	void add(std::string de, std::vector<std::string> en);
	
	
	std::vector<std::string> suche_en(std::string de);
};

#endif
