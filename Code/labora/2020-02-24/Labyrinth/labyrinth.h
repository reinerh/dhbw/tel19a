#ifndef LABYRINTH_H
#define LABYRINTH_H

#include<string>
#include<vector>
#include<iostream>

struct Labyrinth
{
	std::vector<std::string> feld;
	int px;
	int py;	
	
	Labyrinth(std::string filename);
	
	void setPlayerPos();	
	std::string str() const;
	
	bool move_up();
	bool move_right();
	bool move_down();
	bool move_left();
};

std::ostream & operator<<(std::ostream & lhs, Labyrinth const &);

#endif

