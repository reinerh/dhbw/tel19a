#include"labyrinth.h"

#include<fstream>
#include<sstream>
#include<string>
#include<vector>
using namespace std;

Labyrinth::Labyrinth(string filename)
{
	ifstream file(filename);
	string line;
	
	while (getline(file, line))
	{
		feld.push_back(line);
	}
	file.close();
	setPlayerPos();
}

void Labyrinth::setPlayerPos()
{
	for (int y=0; y<feld.size(); y++)
	{
		for (int x=0; x<feld[y].size(); x++)
		{
			if (feld[y][x] == 'S')
			{
				px = x;
				py = y;
			}
		}
	}
}

std::string Labyrinth::str() const
{
	ostringstream out;
	for (auto line : feld)
	{
		out << line << endl;
	}
	return out.str();
}

bool Labyrinth::move_up()
{
	if (py>0 && feld[py-1][px] != '#')
	{
		py--;
		feld[py][px] = 'S';
		return true;
	}
	return false;
}

bool Labyrinth::move_right()
{
	if (px<feld[py].size()-1 && feld[py][px+1] != '#')
	{
		px++;
		feld[py][px] = 'S';
		return true;
	}
	return false;
}

bool Labyrinth::move_down()
{
	if (py<feld.size()-1 && feld[py+1][px] != '#')
	{
		py++;
		feld[py][px] = 'S';
		return true;
	}
	return false;
}

bool Labyrinth::move_left()
{
	if (px>0 && feld[py][px-1] != '#')
	{
		px--;
		feld[py][px] = 'S';
		return true;
	}
	return false;
}

ostream & operator<<(ostream & lhs, Labyrinth const & rhs)
{
	return lhs << rhs.str();
}

