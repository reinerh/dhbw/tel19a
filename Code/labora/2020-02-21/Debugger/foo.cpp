#include<iostream>
using namespace std;

struct foo {
	int a;
	char b;
};

int main()
{
	int x = 42;
	
	foo f;
	
	cout << x << endl;
	
	x = 38;
	
	cout << x << endl;
	cout << f.a << endl;
	
	return 0;
}
