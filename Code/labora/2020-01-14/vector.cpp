#include<iostream>
#include<vector>
#include<string>
using namespace std;

template<typename T>
void print_vector(vector<T> v) {

for (T e : v) {
	cout << e << " ";
}

//	for (int i=0; i<v.size(); i++) {
//		cout << v[i] << " ";
//	}
	cout << endl;
}

int main() {
		
	vector<int> v = { 5, 3, 7, 42 ,38 };
	
	cout << v[3] << endl;
	v[1] = 72;
	cout << v[1] << endl;
	
	print_vector(v);
	v.push_back(104);
	print_vector(v);
	
	vector<string> v2 = {"Hund", "Katze", "Maus"};
	
	print_vector(v2);
	
	return 0;
}