#include<iostream>
#include<string>
#include<vector>
using namespace std;

void print_spielfeld(vector<string> spielfeld);
void eingabe(vector<string> & spielfeld, char s);
bool spalte_voll(vector<string> const & spielfeld);
void run();

// Liest eine Position vom Spieler und trägt an der gegeben Stelle
// das Zeichen s im Spielfeld ein.
void eingabe(vector<string> & spielfeld, char s) {
	// Spielfeld anzeigen
	print_spielfeld(spielfeld);
	
	// Spieler nach seiner Eingabe fragen
	cout << "Bitte ein Feld waehlen: ";
	int input;
	cin >> input;
	input--;
	int zeile = input / 3;
	int spalte = input % 3;
	
	// Eingabe prüfen
	if (input >= 0 && input <= 8) {
		// Spielzug eintragen.
		spielfeld[zeile][spalte] = s;		
	} else {
		cout << "Das war nix" << endl;
		eingabe(spielfeld, s);
	}
}

// Gibt das Spielfeld auf die Konsole aus
void print_spielfeld(vector<string> spielfeld) {
	for (int i = 0; i<spielfeld.size(); i++) {
		string zeile = spielfeld[i];
		cout << " " << zeile[0] 
		     << " | " << zeile[1]
			 << " | " << zeile[2]
			 << endl;
		if (i<2) {
			cout << "---+---+---" << endl;
		}
	}
}

bool spalte_i_voll_mit_s(vector<string> & spielfeld, int i, char s) {
	return spielfeld[0][i] == s &&
	       spielfeld[1][i] == s &&
		   spielfeld[2][i] == s;
}

bool spalte_voll(vector<string> const & spielfeld) {
	return spalte_i_voll_mit_s(spielfeld, 0, 'X') ||
	       spalte_i_voll_mit_s(spielfeld, 1, 'X') ||
		   spalte_i_voll_mit_s(spielfeld, 2, 'X') ||
		   spalte_i_voll_mit_s(spielfeld, 0, 'O') ||
	       spalte_i_voll_mit_s(spielfeld, 1, 'O') ||
		   spalte_i_voll_mit_s(spielfeld, 2, 'O');
}

void run() {
	// Spielfeld erzeugen
	vector<string> spielfeld = { "123", "456", "789" };
	char s = 'X';
	int spielzug = 0;
	
	while (!spalte_voll(spielfeld) &&
	       !zeile_voll(spielfeld) &&
		   !diag_voll(spielfeld) &&
		   spielzug < 9) {   /* Spiel nicht zu Ende */
		// Spieler macht seinen Zug
		eingabe(spielfeld, s);
		
		// Spieler wechseln
		if (s=='X') { s = 'O'; }
		else { s = 'X'; }
		
		// Alternative Art, den Spieler zu wechseln:
		//s = (s=='X' ? 'O' : 'X');
		
		spielzug++;		
	}
	// Auswerten und Gewinner anzeigen.
	if (spielzug < 9) {
		cout << "Gewinner: " << s == 'X' ? 'O' : 'X' << endl;
	}
}

int main() {
	run();
	
	return 0;
}