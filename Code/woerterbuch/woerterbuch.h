#ifndef WOERTERBUCH_H
#define WOERTERBUCH_H

#include<string>
#include<vector>

struct Eintrag {
	std::string de;
	std::string en;
};

struct Woerterbuch {
	std::vector<Eintrag> data;
	
	void add(std::string de, std::string en);
	std::string suche_en(std::string de);
};

#endif
