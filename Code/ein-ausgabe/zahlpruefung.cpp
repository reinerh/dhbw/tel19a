/** Demonstration, wie man eine Eingabe darauf pruefen
    kann, ob sie eine Zahl ist.
 */

#include<iostream>
#include<string>
#include<sstream>
using namespace std;

int getInt()
{
	string s;
	stringstream buffer;
		
	cout << "Bitte geben Sie eine Zahl ein: ";
	getline(cin,s);
	buffer << s;
	
	int result;
	if (!s.empty() && buffer >> result)
	{
		char c;
		if (!(buffer >> c))
		{
			return result;
		}
		cout << "Fehler, unerwartetes Zeichen: " << c << endl;
	}
	else
	{
		cout << "Fehler, Sie haben keine Zahl eingegeben!" << endl;
	}
	cout << "Bitte noch einmal versuchen!" << endl;
	return getInt();

}

int main()
{
	cout << "Mal sehen, ob Sie eine Zahl eingeben können..." << endl << endl;
	cout << getInt() << endl;
	
	return 0;
}