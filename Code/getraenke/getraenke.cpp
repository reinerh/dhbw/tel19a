#include<iostream>
#include<string>
#include<vector>
#include<sstream>
using namespace std;

/** Liest das gewünschte Getränk vom Benutzer ein und
    1: Schreibt "Bier" oder "Cola" in den String.
	2: Liefert den Preis des Getränks zurück.
**/
int read_choice(string & bestelltes_getraenk) {
	cout << "Waehlen Sie ein Getraenk aus (Bier oder Cola): ";
	cin >> bestelltes_getraenk;
	
	if (bestelltes_getraenk == "Bier") { return 100; }
	else if (bestelltes_getraenk == "Cola") { return 200; }
	
	cout << "Eingabe ungueltig." << endl;
	return read_choice(bestelltes_getraenk);
}

/** Liest eine Münzeingabe vom Benutzer und liefert den
    aktualisierten Restbetrag zurück.
**/
int einwurf(int restbetrag) {
	int input;
	
	cout << "Bitte eine Muenze waehlen: " << endl
	     << "  1: 10 Cent" << endl
		 << "  2: 20 Cent" << endl
		 << "  3: 50 Cent" << endl
	     << "  4: 1 Euro" << endl
	     << "  5: 2 Euro" << endl;
	cin >> input;
	
	switch (input) {
		case 1: return restbetrag - 10;
		case 2: return restbetrag - 20;
		case 3: return restbetrag - 50;
		case 4: return restbetrag - 100;
		case 5: return restbetrag - 200;
		default:
		  cout << "Das war keine Muenze." << endl;
		  return einwurf(restbetrag);
	}
}

/** Berechnet das Wechselgeld. **/
string compute_change(int restbetrag) {
	vector<string> wechselgeld;
	restbetrag *= -1;
	
	int zwei_euro = restbetrag / 200;
	restbetrag = restbetrag % 200;
	int ein_euro = restbetrag / 100;
	restbetrag = restbetrag % 100;
	int fuenfzig_cent = restbetrag / 50;
	restbetrag = restbetrag % 50;
	int zwanzig_cent = restbetrag / 20;
	restbetrag = restbetrag % 20;
	int zehn_cent = restbetrag / 10;
	restbetrag = restbetrag % 10;
	
	for (int i=0; i<zwei_euro; i++) {
		wechselgeld.push_back("Zwei Euro");
	}
	for (int i=0; i<ein_euro; i++) {
		wechselgeld.push_back("Ein Euro");
	}
	for (int i=0; i<fuenfzig_cent; i++) {
		wechselgeld.push_back("50 Cent");
	}
	for (int i=0; i<zwanzig_cent; i++) {
		wechselgeld.push_back("20 Cent");
	}
	for (int i=0; i<zehn_cent; i++) {
		wechselgeld.push_back("10 Cent");
	}
	
	cout  << "DEBUG (Restbetrag): " << restbetrag  << endl;
	cout  << "DEBUG (Anzahl Muenzen): " << wechselgeld.size()  << endl;
	
	ostringstream o;
	for (auto s : wechselgeld) {
		o << s << endl;
	}
	return o.str();
}

int main() {
	int restbetrag;
	string bestelltes_getraenk;
	
	restbetrag = read_choice(bestelltes_getraenk);
	
	while (restbetrag > 0) {
		cout << "Noch zu zahlen: " << restbetrag << " Cent" << endl;
		restbetrag = einwurf(restbetrag);
	}
	
	cout << bestelltes_getraenk << endl;
	cout << "Wechselgeld: "
	     << compute_change(restbetrag)
		 << endl;
	
	return 0;
}