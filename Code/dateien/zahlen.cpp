#include<fstream>
using namespace std;

int main()
{
	ifstream infile("zahlen.txt");
	ofstream outfile("sterne.txt");
	
	int zahl;
	while (infile >> zahl && zahl > 0)
	{
		for (int x=0; x<zahl; x++)
		{
			outfile << '*';
		}
		outfile << endl;
	}
	infile.close();
	outfile.close();
	
	return 0;
}