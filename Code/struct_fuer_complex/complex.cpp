#include <iostream>
#include <string>
#include <sstream>
using namespace std;


struct Complex {
    double real;
    double img;
    
    string to_string() const;
	Complex add(Complex const & other) const;
	Complex mult(Complex const & other) const;
    
};

string Complex::to_string() const {
    ostringstream s;
    
    // "real + img i" zurückliefern
    s << real << " + " << img << "i";
    return s.str();
}

Complex operator+(Complex const & left, Complex const & right) {
	return left.add(right);
}

Complex operator*(Complex const & left, Complex const & right) {
	return left.mult(right);
}

ostream & operator<<(ostream & left, Complex const & right) {
	left << right.to_string();
	return left;
}




Complex Complex::add(Complex const & other) const {
	Complex result = { real + other.real, img + other.img };
	return result;
}

Complex Complex::mult(Complex const & other) const {
	Complex result = { 
	    real * other.real - img * other.img, 
		real * other.img + img * other.real };
		
	return result;
}



int main() {
	
	Complex c = {3, 4.2};
	Complex d = {5.3, 6};
	
	Complex e = c+d;   // e = c + d
	Complex f = c*d;  // f = c * d
	
	cout << e << endl;
	
	cout << f << endl;
	
	
	return 0;
	
}