#include<iostream>
#include<string>
#include<vector>
#include<sstream>
using namespace std;

struct Spielfeld {
	// Konstruktor
	Spielfeld(int zeilen, int spalten);
	
	//Datenfelder für ein Spielfeld
	vector<string> data;
	
	string to_string() const;
};

ostream & operator<<(ostream &, Spielfeld const &);

int main() {
	
	Spielfeld s(5,4);
	
	cout << s;
	
	return 0;
}

Spielfeld::Spielfeld(int zeilen, int spalten)
: data(zeilen,string(spalten,' '))
{}

// Spielfeld::Spielfeld(int zeilen, int spalten)
// {
	// for (int z = 0; z<zeilen; z++) {
		// data.push_back("");
		// for (int s=0; s<spalten; s++){
			// data[z].push_back(' ');
		// }
	// }
// }

string Spielfeld::to_string() const
{
	ostringstream s;
	for (int i=0; i<data.size(); i++) {
		s << data[i] << endl;
	}
	return s.str();
}

ostream & operator<<(ostream & left, Spielfeld const & right)
{
	return left << right.to_string();
}
