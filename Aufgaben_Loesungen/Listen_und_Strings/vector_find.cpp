#include<iostream>
#include<vector>
using namespace std;

/*** AUFGABE: Suchen von Elementen in einem Vektor ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine Funktion `find()`, die als Argument einen Vektor `v` aus Zahlen und eine einzelne Zahl `x` erwartet.
Die Funktion soll die Position von `x` in `v` bestimmen und zurückgeben. Kommt `x` nicht vor, soll die Länge von `v` geliefert werden.
***/
int find(vector<int> v, int x) {
    for (int i=0; i<v.size(); i++) {
        if (v[i] == x) {
            return i;
        }
    }
    return v.size();

/*** TESTCODE/MAIN ***/
int main()
{
    vector<int> v1 = { 1, 3, 5, 5, 7, 9, 11 };
    
    cout << find(v1, 5) << endl;   // Soll '2' ausgeben.
    cout << find(v1, 1) << endl;   // Soll '0' ausgeben.
    cout << find(v1, 42) << endl;  // Soll '7' ausgeben.
}
