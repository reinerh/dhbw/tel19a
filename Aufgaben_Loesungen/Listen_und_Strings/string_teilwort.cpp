#include<iostream>
#include<string>
using namespace std;

/*** AUFGABE: Suchen von Teilwörtern in einem String ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine Funktion `contains()`, die als Argumente zwei Strings s1 und
s2 erwartet. Die Funktion soll 'true' zurückliefern, falls s2 in s1 vorkommt.
Ansonsten soll sie 'false' liefern.
***/
bool contains(string s1, string s2) {
    if (s1.size() < s2.size()) { return false; }
    for (int i=0; i<=s1.size()-s2.size(); i++) {
        string h;
        for (int j=0; j<s2.size(); j++) {
            h.push_back(s1[i+j]);
        }
        if (h == s2) { return true; }
    }
    return false;
}

/*** TESTCODE/MAIN ***/
int main()
{
    cout << contains("Hallo", "Ha") << endl;      // Soll 1 ausgeben.
    cout << contains("Hallo", "Hallo") << endl;   // Soll 1 ausgeben.
    cout << contains("Hallo", "Hb") << endl;      // Soll 0 ausgeben.
    cout << contains("Ha", "Hallo") << endl;      // Soll 0 ausgeben.
    cout << contains("Hallo", "") << endl;        // Soll 1 ausgeben.
    cout << contains("", "Hallo") << endl;        // Soll 0 ausgeben.
}
