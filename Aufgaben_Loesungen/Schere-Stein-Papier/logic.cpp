#include "logic.h"
#include "ui.h"

void run(std::string p1, std::string p2, int rounds)
{
    while (rounds > 0) {        
        Move m1 = get_move(p1);
        Move m2 = get_move(p2);
        
        Result res = evaluate(m1,m2);
        switch (res) {
            case Result::p1: print_winner(p1); break;
            case Result::p2: print_winner(p2); break;
            default: print_draw();
        }
        --rounds;
    }
}

Result evaluate(Move m1, Move m2)
{
    if (m1 == m2) { return Result::draw; }
    if (m1 == Move::rock && m2 == Move::scissors) { return Result::p1; }
    if (static_cast<int>(m1) > static_cast<int>(m2)) { return Result::p1; }
    return Result::p2;
}