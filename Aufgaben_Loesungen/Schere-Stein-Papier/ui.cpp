#include "ui.h"

#include<iostream>
using namespace std;

std::string to_string(Move m) {
    switch (m) {
        case Move::rock: return "Stein";
        case Move::paper: return "Papier";
        case Move::scissors: return "Schere";
    }
    return "";
}

Move get_move(std::string player_name)
{
    cout << "Spieler " << player_name << ", bitte waehlen Sie einen Zug:" << endl;
    for (auto m : { Move::rock, Move::paper, Move::scissors }) {
        cout << "  " << static_cast<int>(m)+1 << ": " << to_string(m) << endl;
    }
    int input;
    cin >> input;
    if (input > 0 && input < 4) { return static_cast<Move>(input); }
    else {
        cout << "Die Eingabe war ungueltig. Bitte noch einmal versuchen." << endl << endl;
        return get_move(player_name);
    }
}

void print_winner(std::string player_name)
{
    cout << player_name << " hat gewonnen!" << endl << endl;
}

void print_draw()
{
    cout << "Unentschieden!" << endl << endl;
}