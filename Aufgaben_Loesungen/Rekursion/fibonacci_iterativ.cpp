#include<iostream>
using namespace std;

/*** AUFGABE: Suchen von Teilwörtern in einem String ***/

/*** AUFGABENSTELLUNG:
Schreiben Sie eine iterative Funktion `fib()`, die als Argument eine positive Zahl `n` erwartet.
Die Funktion soll die n-te Fibonacci-Zahl zurückliefern.

Anmerkung: Im Gegensatz zur anderen Fibonacci-Aufgabe sollen Sie hier keine Rekursion schreiben,
           sondern das gleiche mittels einer Schleife bewerkstelligen. D.h. Sie müssen sich
           überlegen, welche Zwischenergebnisse Sie von einem zum nächsten Schleifendurchlauf
           mitnehmen müssen. Dies soll verdeutlichen, warum Rekursion manchmal die bessere
           und einfachere Wahl ist.
***/
unsigned int fib(unsigned int n) {
    unsigned int x1 = 1, x2 = 1;
    for (int i=2; i<=n; i++) {
        auto h = x1 + x2;
        x1 = x2;
        x2 = h;
    }    
    return x2;
}
/*** TESTCODE/MAIN ***/
int main()
{
    cout << fib(0) << endl;      // Soll 1 ausgeben.
    cout << fib(1) << endl;      // Soll 1 ausgeben.
    cout << fib(2) << endl;      // Soll 2 ausgeben.
    cout << fib(3) << endl;      // Soll 3 ausgeben.
    cout << fib(4) << endl;      // Soll 5 ausgeben.
    cout << fib(5) << endl;      // Soll 8 ausgeben.
    cout << fib(6) << endl;      // Soll 13 ausgeben.
    
    return 0;
}
